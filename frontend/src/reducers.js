import {combineReducers} from "redux";
import productReducer from "./app/reducers/productReducer";

const reducers = combineReducers({
	product: productReducer
});
export default reducers;
