const initState = {
	products: [],
	orderItems: []
};

export default (state = initState, action) => {
	switch (action.type) {
		case 'GET_PRODUCTS':
			return {
				...state,
				products: action.products
			};
		case  'ADD_ITEM':
			if (action.status === 201) {
				alert("已添加成功!");
			}
			return {
				...state
			};
		case 'GET_ORDER_ITEMS':
			return  {
				...state,
				orderItems: action.orderItems
			};
		case 'DELETE_ITEM':
			if(action.status === 404){
				alert('商品删除失败，请稍后再试')
			}
			return {
				...state
			}
		case 'CREATE_PRODUCT':
			if(action.status === 409){
				alert('商品名称已存在，请输入新的商品名称')
			}
			return {
				...state
			}
		default:
			return state;
	}
};
