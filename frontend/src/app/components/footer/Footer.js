import React from 'react';
import './Footer.less'

function Footer(props) {
	return (
		<div className={'footer'}>
			<p className="foot-label">TW Mall &copy;2018 Created by ForCheng</p>
		</div>
	);
}

export default Footer;
