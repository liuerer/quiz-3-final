import React, {Component} from 'react';
import './OrderDetail.less';

class OrderDetail extends Component {

	handleOnClick(event){
		this.props.deleteItem(event.target.value, this.props.getItems);
	}

	render() {
		const orderItemList = this.props.orderItems.map((item) => {
			const {id, productName, price, unit, quantity} = item;
			return (
				<tr key={id.toString()}>
					<td>{productName}</td>
					<td>{price}</td>
					<td>{quantity}</td>
					<td>{unit}</td>
					<td><button onClick={this.handleOnClick.bind(this)} value={id}>删除</button></td>
				</tr>
			);
		});


		return (
			<div className={'order-detail'}>
				<table className={'show'}>
					<thead>
					<tr>
						<th>名字</th>
						<th>单价</th>
						<th>数量</th>
						<th>单位</th>
						<th>操作</th>
					</tr>
					</thead>
					<tbody>{orderItemList}</tbody>
				</table>
			</div>
		);
	}
}

export default OrderDetail;
