import React, {Component} from 'react';
import {Link} from "react-router-dom";
import { MdAccountBalance, MdShoppingCart, MdAdd } from 'react-icons/md';
import './Header.less'
import OrderDetail from "../../pages/orderDetatil/Order";

class Header extends Component {
	render() {
		return (
			<div className={'header'}>
				<Link to={'/'}><nav><MdAccountBalance /><p>商城</p></nav></Link>
				<Link to={'/order'}><nav><MdShoppingCart /><p>订单</p></nav></Link>
				<Link to={'/create'}><nav><MdAdd /><p>添加商品</p></nav></Link>
			</div>
		);
	}
}

export default Header;
