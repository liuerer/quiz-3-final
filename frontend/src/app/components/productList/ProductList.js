import React, {Component} from 'react';
import './ProductList.less'

class ProductList extends Component {


	handleOnClick(event){
		this.props.addItem(event.target.value);
	}

	render() {
		const productList = this.props.products.map((item) => {
			const {id, imgUri, productName, price, unit} = item;
			return (
				<li key={id.toString()}>
					<section className={'product'}>
						<img src={imgUri} alt={'图片不存在'}/>
						<p className={'name'}>{productName}</p>
						<p className={'price'}>单价:{price}元/{unit}</p>
						<button value={id} onClick={this.handleOnClick.bind(this)}>+</button>
					</section>
				</li>
			);
		});

		return (
			<ul className={'product-list'}>
				{productList}
			</ul>
		);
	}
}

export default ProductList;
