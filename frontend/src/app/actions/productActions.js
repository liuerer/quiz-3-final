export const getProducts = () => (dispatch) => {
	fetch('http://localhost:8080/api/products', {method: 'GET'})
		.then(response => response.json())
		.then(result => {
			dispatch({
				type: 'GET_PRODUCTS',
				products: result
			});
		})
		.catch(alert);
};

export const addItem = (productId) => (dispatch) => {
	let item = {
		'productId': productId
	};
	fetch('http://localhost:8080/api/order/item',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
			body: JSON.stringify(item)
		})
		.then(response => {
			dispatch({
				type: 'ADD_ITEM',
				status: 201
			});
		})
		.catch(alert);
};

export const getItems = () => (dispatch) => {
	fetch('http://localhost:8080/api/order/items', {method: 'GET'})
		.then(response => response.json())
		.then(result => {
			dispatch({
				type: 'GET_ORDER_ITEMS',
				orderItems: result
			});
		})
		.catch(alert);
};

export const deleteItem = (orderItemId, callback) => (dispatch) => {
	let item = {
		'orderItemId': orderItemId
	};
	fetch('http://localhost:8080/api/order/item',
		{
			method: 'DELETE',
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
			body: JSON.stringify(item)
		})
		.then(response => {
			dispatch({
				type: 'DELETE_ITEM',
				status: response.status
			});
		})
		.then(callback);
};

export const createProduct = (product) => (dispatch) => {
	fetch('http://localhost:8080/api/product',
		{
			method: 'POST',
			headers: {
				'Content-Type': 'application/json;charset=UTF-8',
			},
			body: JSON.stringify(product)
		})
		.then(response => {
			dispatch({
				type: 'CREATE_PRODUCT',
				status: response.status
			});
		})
		.catch(alert);
};
