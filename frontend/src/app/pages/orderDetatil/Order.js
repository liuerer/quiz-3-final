import React, {Component} from 'react';
import './Order.less'
import {connect} from "react-redux";
import OrderDetail from "../../components/orderDetail/OrderDetail";
import {getItems, deleteItem} from "../../actions/productActions";
import {bindActionCreators} from "redux";


class Order extends Component {

	componentDidMount() {
		this.props.getItems();
	}

	render() {
		console.log(this.props.orderItems)
		const orderShow = () => {
			if(this.props.orderItems.length === 0){
				return <p>暂无订单，返回商城页面继续购买</p>
			}
			return <OrderDetail orderItems={this.props.orderItems} deleteItem={this.props.deleteItem} getItems={this.props.getItems}/>
		}
		return (
			<div className={'order'}>
				{orderShow()}
			</div>
		);
	}
}

const mapStateToProps = state => ({
	orderItems: state.product.orderItems
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getItems,
	deleteItem
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Order);
