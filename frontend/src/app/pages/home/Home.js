import React from 'react'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getProducts, addItem} from "../../actions/productActions";
import ProductList from "../../components/productList/ProductList";
import './Home.less'

class Home extends React.Component {

	componentDidMount() {
		this.props.getProducts();
	}

	render() {
		return (
			<div className={'home'}>
				<ProductList products={this.props.products} addItem={this.props.addItem}/>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	products: state.product.products
});

const mapDispatchToProps = dispatch => bindActionCreators({
	getProducts,
	addItem
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
