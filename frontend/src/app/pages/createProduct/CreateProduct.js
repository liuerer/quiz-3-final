import React, {Component} from 'react';
import {connect} from "react-redux";
import {createProduct} from "../../actions/productActions";
import {bindActionCreators} from "redux";
import './CreateProduct.less'

class CreateProduct extends Component {
	constructor(props){
		super(props);
		this.state={
			productName: '',
			price: 0,
			unit: '',
			imgUri: ''
		}
	}

	handleOnChangeName(event){
		this.setState({
			productName: event.target.value
		});
	}
	handleOnChangePrice(event){
		this.setState({
			price: event.target.value
		});
	}
	handleOnChangeUnit(event){
		this.setState({
			unit: event.target.value
		});
	}
	handleOnChangeUrl(event){
		this.setState({
			imgUri: event.target.value
		});
	}

	handleOnClick(){
		this.props.createProduct(this.state);
	}

	render() {
		return (
			<div className={'create'}>
				<h3>添加商品</h3>
				<label><span>*</span><p>名称:</p></label>
				<input type={'text'} id={'name'} placeholder={'名称'} onChange={this.handleOnChangeName.bind(this)}/>
				<label><span>*</span><p>价格:</p></label>
				<input type={'text'} id={'price'} placeholder={'价格'} onChange={this.handleOnChangePrice.bind(this)}/>
				<label><span>*</span><p>单位:</p></label>
				<input type={'text'} id={'unit'} placeholder={'单位'} onChange={this.handleOnChangeUnit.bind(this)}/>
				<label><span>*</span><p>图片:</p></label>
				<input type={'text'} id={'url'} placeholder={'URL'} onChange={this.handleOnChangeUrl.bind(this)}/>
				<button onClick={this.handleOnClick.bind(this)}>提交</button>
			</div>
		);
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({
	createProduct
}, dispatch);

export default connect(undefined, mapDispatchToProps)(CreateProduct);
