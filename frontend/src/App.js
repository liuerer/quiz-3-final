import React, {Component} from 'react'
import {BrowserRouter as Router} from "react-router-dom";
import {Switch, Route} from "react-router";

import Header from "./app/components/header/Header";
import './App.less'
import Home from "./app/pages/home/Home";
import Footer from "./app/components/footer/Footer";
import OrderDetail from "./app/pages/orderDetatil/Order";
import CreateProduct from "./app/pages/createProduct/CreateProduct";

class App extends Component {
	render() {
		return (
			<Router>
				<div className={'App'}>
					<Header />
					<Switch>
						<Route exact path={'/order'} component={OrderDetail} />
						<Route exact path={'/create'} component={CreateProduct} />
						<Route exact path={'/'} component={Home} />
					</Switch>
					<Footer />
				</div>
			</Router>
		);
	}
}

export default App;
