import React from 'react';
import App from './App';
import {Provider} from "react-redux";
import store from "./store";
import ReactDom from 'react-dom'

ReactDom.render(
	<Provider store={store}>
		<App/>
	</Provider>,
	document.getElementById('root')
);
