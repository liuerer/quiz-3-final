package com.twuc.webApp.web;

import com.twuc.webApp.contract.CreateProductRequest;
import com.twuc.webApp.contract.DeleteItemRequest;
import com.twuc.webApp.contract.AddItemRequest;
import com.twuc.webApp.domain.OrderItem;
import com.twuc.webApp.domain.OrderItemRepository;
import com.twuc.webApp.domain.Product;
import com.twuc.webApp.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
@CrossOrigin(origins = "*", allowedHeaders = "*", exposedHeaders = {"Location"})
public class MallController {

	@Autowired
	ProductRepository productRepo;
	
	@Autowired
	OrderItemRepository itemRepo;

	@GetMapping("/products")
	public ResponseEntity getProducts(){

		List<Product> products = productRepo.findAll(Sort.by(Sort.Direction.ASC, "id"));

		return ResponseEntity.ok(products);
	}

	@PostMapping("/order/item")
	public ResponseEntity addItem(@RequestBody @Valid AddItemRequest request){
		Optional<Product> product = productRepo.findById(request.getProductId());
		if(!product.isPresent()){
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Optional<OrderItem> orderItem = itemRepo.findByProductId(request.getProductId());
		if(!orderItem.isPresent()){
			itemRepo.save(new OrderItem(product.get()));
		} else {
			orderItem.get().addItem();
			itemRepo.save(orderItem.get());
		}
		
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
	
	@GetMapping("/order/items")
	public ResponseEntity getOrderItems(){
		List<OrderItem> orderItems = itemRepo.findAll(Sort.by(Sort.Direction.ASC, "id"));
		
		return ResponseEntity.ok(orderItems);
	}
	
	@DeleteMapping("/order/item")
	public ResponseEntity deleteItem(@RequestBody @Valid DeleteItemRequest request){
		Optional<OrderItem> orderItem = itemRepo.findById(request.getOrderItemId());
		if(!orderItem.isPresent()){
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		itemRepo.deleteById(request.getOrderItemId());
		
		return ResponseEntity.ok().build();
	}
	
	@PostMapping("/product")
	public ResponseEntity createProduct(@RequestBody @Valid CreateProductRequest request){
		Optional<Product> product = productRepo.findByProductName(request.getProductName());
		if(product.isPresent()){
			return ResponseEntity.status(HttpStatus.CONFLICT).build();
		}
		Product createProduct = new Product(request.getProductName(),request.getPrice(),request.getUnit(),request.getImgUri());
		productRepo.save(createProduct);
		
		return ResponseEntity.status(HttpStatus.CREATED).build();
	}
}

