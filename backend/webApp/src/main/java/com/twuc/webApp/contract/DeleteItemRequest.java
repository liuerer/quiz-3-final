package com.twuc.webApp.contract;

public class DeleteItemRequest {
	
	private Long orderItemId;
	
	public DeleteItemRequest() {
	}
	
	public DeleteItemRequest(Long orderItemId) {
		this.orderItemId = orderItemId;
	}
	
	public Long getOrderItemId() {
		return orderItemId;
	}
	
	public void setOrderItemId(Long orderItemId) {
		this.orderItemId = orderItemId;
	}
}
