package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity(name = "order_items")
public class OrderItem {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "product_name", nullable = false, length = 128)
	private String productName;
	
	@Column(nullable = false)
	private int price;
	
	@Column(nullable = false, length = 128)
	private String unit;
	
	@Column(nullable = false)
	private int quantity = 0;
	
	@ManyToOne
	private Product product;
	
	public OrderItem() {
	}
	
	public OrderItem(Product product) {
		this.productName = product.getProductName();
		this.price = product.getPrice();
		this.unit = product.getUnit();
		this.quantity++;
		this.product = product;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public int getPrice() {
		return price;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public int getQuantity() { return quantity; }
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public void addItem() {
		this.quantity++;
	}
}
