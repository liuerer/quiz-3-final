package com.twuc.webApp.domain;


import javax.persistence.*;

@Entity(name = "products")
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "product_name", nullable = false, length = 128)
	private String productName;
	
	@Column(nullable = false)
	private int price;
	
	@Column(nullable = false, length = 128)
	private String unit;
	
	@Column(name = "img_uri", nullable = false, length = 250)
	private String imgUri;
	
	public Product() {
	}
	
	public Product(String productName, int price, String unit, String imgUri) {
		this.productName = productName;
		this.price = price;
		this.unit = unit;
		this.imgUri = imgUri;
	}
	
	public Long getId() {
		return id;
	}
	
	public String getProductName() {
		return productName;
	}
	
	public int getPrice() {
		return price;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public String getImgUri() {
		return imgUri;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public void setImgUri(String imgUri) {
		this.imgUri = imgUri;
	}
}
