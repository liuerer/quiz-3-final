package com.twuc.webApp.contract;

import com.twuc.webApp.domain.Product;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

public class CreateProductRequest {
	
	private String productName;
	private int price;
	private String unit;
	private String imgUri;
	
	public CreateProductRequest() {
	}
	
	public CreateProductRequest(Product product) {
		this.productName = product.getProductName();
		this.price = product.getPrice();
		this.unit = product.getUnit();
		this.imgUri = product.getImgUri();
	}
	
	public String getProductName() {
		return productName;
	}
	
	public void setProductName(String productName) {
		this.productName = productName;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}
	
	public String getUnit() {
		return unit;
	}
	
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	public String getImgUri() {
		return imgUri;
	}
	
	public void setImgUri(String imgUri) {
		this.imgUri = imgUri;
	}
}
