package com.twuc.webApp.contract;

public class AddItemRequest {
	
	private Long productId;
	
	public AddItemRequest() {
	}
	
	public AddItemRequest(Long productId) {
		this.productId = productId;
	}
	
	public Long getProductId() {
		return productId;
	}
	
	public void setProductId(Long productId) {
		this.productId = productId;
	}
}
