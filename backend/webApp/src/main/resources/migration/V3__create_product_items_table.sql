create table if not exists order_items
(
    id           bigint       not null AUTO_INCREMENT,
    product_name varchar(128) not null,
    price        int          not null,
    quantity     int          not null,
    unit         varchar(128) not null,
    product_id   bigint       not null,
    primary key (id),
    foreign key (product_id) references products(id)
) character set = utf8;
