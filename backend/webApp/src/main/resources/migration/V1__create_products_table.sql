create table if not exists products(
    id      bigint not null AUTO_INCREMENT,
    img_uri varchar(250) not null,
    product_name    varchar(128) not null,
    price   int      not null,
    unit    varchar(128) not null,
    primary key (id)
)character set = utf8;
